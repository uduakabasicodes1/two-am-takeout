This file is for taking notes as you go.

Initial: 20-30mins looking at code
- Vue is very interesting (the structure)
- layout is on top, functionality + calls to server made at the bottom
- makes we wonder if this is emphasis on design first, functionality later(gets in where it fits in)
- Line 40 of App.vue --- pass props??
- [store/index.js] ---> reminds me of Redux but for Vue? (must look at documentation for Vuex) if so, this is pretty cool, less setup than Redux in React
- mutations vs actions []

BUG 1: About 10 - 15 mins, mostly spent familiarizing myself with the code 
- debugging line 153 (was initally 117?) data is an object returning backendHealthy, connection, currentMessage, currentName, errorMessage, messages, snackbar, totalChatHeight & uuid which is why we get "backend check failed, response code was 200, data was [object Object]"

- line 12 in handlers.go --> I typed health into the search bar and checked all the files it was in 
(helped me with familiarizing myself with the code)
then ran go test./server -v and saw the issue was with line 12 in handlers.go; remembered seeing that "ok" was in main_test.go; compared the two and saw that it was capitilized in handlers.go but it was checking for lowercase via line 55 of main_test.go [case sensitivity]; also checking http://localhost:3000/health to verify the status

- line 159 in Chat.vue allows me to see what data is exactly [console.log() helps me identify what im not sure of]
- line 161 in Chat.vue allows me to see it as observer obj in the console

BUG 2: 
<!-- There is a test for the backend where the assumptions about what needs to be tested is incorrect. The test passes, but doesn't guarantee compatibility with how the frontend expects to communicate. You need to think about how the two interact and are compatible, and change the test code to match. -->
- client side SHOULD take in text + name as an object;
-- what do we know? [reading code + overthinking + overlooking]
-- confused on this, will come back

FRONTEND 1: About 20 minutes (styling took up bulk of the time) 
- user is able to now enter name, however I'd love to keep track of the user's name so they don't have to keep re-entering the name/username but that may require user authentication or I'd just keep track of the state [useState hook] or [redux]

-- looked at documentation for vuetify // a few components I saw that I thought were pretty cool...
-- I liked their version of modals 

-- How to add css files in vue? [i like to do basic styling in pure css, easier to mainpulate, I like to create containers + divs + sections using frameworks because it makes for a faster setup when it comes to mapping out responsiveness]
<!-- https://stackoverflow.com/questions/43784202/how-to-include-css-files-in-vue-2 -->

- styling needs to be done as message is appearing first.. display grid and then set grid-template-columns to 1fr or grid-template-rows to 1fr 1fr so that message can be above the username/name + would give message a bigger font + name a smaller font with a lighter color but not too light due to accessibilty 

- basic styling complete 


BUG 2: andddd WE'RE BACK and pushing the hour and a half mark sso lets look at documentation
<!-- Websocket documentation -->
WebSocket additionally supports Binary requests ("Blob", "Array Buffer" and "Byte Buffer")
byte --> in main_test.go
(interesting)


<!-- websocket: the client is not using the websocket protocol: 'upgrade' token not found in 'Connection' header -->
the error I get in backend terminal when i try to access ws://localhost:3000/ws

I've used socket.io in a project but never created my own tests. I know that the client starts off by making a request to the server and then the server sends back a response (req, res, next) ---> thinking back on my sql queries 

however, from what I'm able to understand from the code from TestWebsockets, there is no line of code initializing the socket... 
it calls for the path but this is only testing to make sure a message can be read correctly in the way its formatted... 

Are both sender and reader supposed to .Close() at the same time? That's interesting. 

saw a 101 status "Switching Protocols"  ---> why and how is it still working.... look more into this 
-- does this possibly have anything to do with the bonus [security issue??]
a. Connection Start: Stalled
b. Request/Response
<!-- Content Download. The browser is receiving the response -->
Caution; request is not finished yet
-----??????
<!-- GET request needs to be made to start a new websocket  -->
----thoughts all over the place
sendMethod function is allowing client to send message OBJECT
<!-- {text, name, uuid} -->
 Client is sending a JSON string but in the server, TestWebsockets function is just testing for the text formatting of the messages and checking to see if they are being read correctly... why are we not sending back a response?
[ needs to resembles the JSON of what the client is sending]

<!-- pushing 2 hours, not sure how to code this out -->

-- looking and comparing main_test.go to client.go @ for WriteMessage function

<!-- was looking at documentation and read up on -->
Status Code: 101 Web Socket Protocol Handshake
Client handshake request --- 


BONUS: SECURITY BUG 
- "This page has a non-HTTPS secure origin."
- could this be about ws versus wss, as ws is for HTTP and wss is better suited for wss??
- some sort of function that says if we're not on local host, use wss instead (??)
<!-- I wish I could find the article I was reading, lost in tabs -->

BONUS: STYLING 
-- did this earlier when creating the username text box 
-- as UPchieve is for students/mentors... if I had time, I'd make it a bit more playful with various colored blobs to imitate prints


<!-- ALL TESTS PASSING EXCEPT FOR SECURITY BUG... still need to look into documentation for that but I'm pass the 2 hour mark! -->


<!-- Saturday, 2/27 1:20pm to 2:10 -->
looking back at code for 2nd bug... passed through additonal dummy data for verification + console.log uuid 
dummy data showing in terminal

