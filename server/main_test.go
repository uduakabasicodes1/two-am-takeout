package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"testing"
	"time"
	"fmt"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"

	"gitlab.com/upchieve/two-am-takeout/server/handlers"
)

var ctx context.Context
var cancel context.CancelFunc

// what is struct?? 

// struct - a composite data type declaration that defines a 
// physically grouped list of variables under one name in a block of 
// memory, allowing the different variables to be accessed via a single 
// pointer or by the struct declared name which returns the same address

// ServerSuite created and passed thru SetupSuite, TeardownSuite & TestHealthCheck
type ServerSuite struct {
	suite.Suite
	ctx    context.Context
	cancel context.CancelFunc
}

// making message into stringified json

type NewUserMessage struct {
	Text string `json:"text"`
	Name string `json:"name"`
	UUID string `json:"uuid"`
}

func (suite *ServerSuite) SetupSuite() {
	ctx = context.Background()
	suite.ctx, suite.cancel = context.WithCancel(ctx)

	go startServer(ctx)

	time.Sleep(1000)
}

func (suite *ServerSuite) TeardownSuite() {
	suite.cancel()
}

func (suite *ServerSuite) TestHealthCheck() {
	resp, err := http.Get("http://localhost:3000/health")
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), http.StatusOK, resp.StatusCode)

	respData, err := ioutil.ReadAll(resp.Body)
	assert.Nil(suite.T(), err)

	var respBody handlers.HealthBody //
	err = json.Unmarshal(respData, &respBody) //taking in respData from line 60 ---> now line 66
	// what is Unmarshal??
	// Go's terminology calls marshal the process of generating a JSON string 
	// from a data structure, and unmarshal the act of parsing JSON to a data structure
	assert.Nil(suite.T(), err)

	// make sure health body works matches what the frontend is expecting
	respString := string(respData)
	assert.Contains(suite.T(), respString, "\"ok\":true")
}

func (suite *ServerSuite) TestWebsockets() {
	u := url.URL{Scheme: "ws", Host: "localhost:3000", Path: "/ws"}

	reader, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer reader.Close()
	assert.Nil(suite.T(), err)

	sender, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	defer sender.Close()
	assert.Nil(suite.T(), err)

	newmessage := NewUserMessage{"Halo, Websocket!", "Kris", "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000"}
	byte, err := json.Marshal(newmessage)
	fmt.Println(string(byte))
	assert.Nil(suite.T(), err)

	sender.WriteMessage(websocket.TextMessage, byte)

	// not seeing Hello, Websocket in console// server isnt returning response it seems

	// checking that text formatted messages go through and can be read correctly
	// sender.WriteMessage(websocket.TextMessage, []byte("Hello, Websocket!"))

	_, message, err := reader.ReadMessage()
	// trying to follow test setup in TestHealthCheck lines 60-72 (now 58-76) because thats response of data + body
	var respBody NewUserMessage // needs a handler if im following TestHealthCheck //created thru struct in this file
	// because when I tried to do it thru handlers.go, it only worked up until trying to pass it thru json.Marshal
	// what exacly is handler doing?
	err = json.Unmarshal(message, &respBody) //taking in message from line 97 ---> now 101
	assert.Nil(suite.T(), err)
	assert.Equal(suite.T(), newmessage, respBody)

	// comparing to TestHealthCheck but client is sending an object... to then stringify data 
	// [respString := string(respData)
	// assert.Contains(suite.T(), respString, "\"ok\":true")]
	
	// assert.Equal(suite.T(), "Hello, Websocket!", string(message))

}

func TestServerSuite(t *testing.T) {
	suite.Run(t, new(ServerSuite))
}