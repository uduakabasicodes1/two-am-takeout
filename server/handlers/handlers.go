package handlers

import (
	"encoding/json"
	"net/http"
	"fmt"
	"gitlab.com/upchieve/two-am-takeout/server/sockets"
)

// HealthBody is the response body for a health check
type HealthBody struct {
	OK bool `json:"ok"`
}

// no longer needed since i had to create it directly in main_test.go file because it still needed to be destructured
// type NewUserMessage struct {
// 	// Text string `json:"text"`
// 	Text string `json:"ok"`
// }

// ServeHealth handles healthcheck requests
func ServeHealth(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8080")
	json.NewEncoder(w).Encode(HealthBody{OK: true})
}

// ServeSocket returns a socket handler
func ServeSocket(h *sockets.Hub) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Hello Server Socket") //if i was able to keep the NewUserMessage in this file, I would have tried to pass it thru here like how its setup on line 24
		sockets.ServeWs(h, w, r)
	}
}
